// ----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2024 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include "styles.h"
#include "png2img.h"


XftColor      *XftColors [NXFTCOLORS];
XftFont       *XftFonts [NXFTFONTS];

X_textln_style tstyle1;

XImage     *inputsect;
XImage     *shuffsect;
XImage     *lfshfsect;
XImage     *redzita_img;
RotaryGeom  inpbal_geom;
RotaryGeom  hpfilt_geom;
RotaryGeom  shfreq_geom;
RotaryGeom  shgain_geom;
RotaryGeom  lffreq_geom;
RotaryGeom  lfgain_geom;



int styles_init (X_display *disp, X_resman *xrm)
{
    XftColors [C_MAIN_BG] = disp->alloc_xftcolor (0.25f, 0.25f, 0.25f, 1.0f);
    XftColors [C_MAIN_FG] = disp->alloc_xftcolor (1.0f, 1.0f, 1.0f, 1.0f);
    XftColors [C_TEXT_BG] = disp->alloc_xftcolor (1.0f, 1.0f, 0.0f, 1.0f);
    XftColors [C_TEXT_FG] = disp->alloc_xftcolor (0.0f, 0.0f, 0.0f, 1.0f);

    XftFonts [F_TEXT] = disp->alloc_xftfont (xrm->get (".font.text", "luxi:bold::pixelsize=11"));

    tstyle1.font = XftFonts [F_TEXT];
    tstyle1.color.normal.bgnd = XftColors [C_TEXT_BG]->pixel;
    tstyle1.color.normal.text = XftColors [C_TEXT_FG];

    inputsect = png2img (SHARED"/inputsect.png", disp, XftColors [C_MAIN_BG]);
    shuffsect = png2img (SHARED"/shuffsect.png", disp, XftColors [C_MAIN_BG]);
    lfshfsect = png2img (SHARED"/lfshfsect.png", disp, XftColors [C_MAIN_BG]);
    redzita_img  = png2img (SHARED"/redzita.png",  disp, XftColors [C_MAIN_BG]); 
    if (!inputsect || !shuffsect || !lfshfsect || !redzita_img) return 1;

    inpbal_geom._backg = XftColors [C_MAIN_BG];
    inpbal_geom._image [0] = inputsect;
    inpbal_geom._lncol [0] = 1;
    inpbal_geom._x0 = 28;
    inpbal_geom._y0 = 17;
    inpbal_geom._dx = 25;
    inpbal_geom._dy = 25;
    inpbal_geom._xref = 12.5;
    inpbal_geom._yref = 12.5;
    inpbal_geom._rad = 12;

    hpfilt_geom._backg = XftColors [C_MAIN_BG];
    hpfilt_geom._image [0] = inputsect;
    hpfilt_geom._lncol [0] = 0;
    hpfilt_geom._x0 = 87;
    hpfilt_geom._y0 = 17;
    hpfilt_geom._dx = 25;
    hpfilt_geom._dy = 25;
    hpfilt_geom._xref = 12.5;
    hpfilt_geom._yref = 12.5;
    hpfilt_geom._rad = 12;

    shgain_geom._backg = XftColors [C_MAIN_BG];
    shgain_geom._image [0] = shuffsect;
    shgain_geom._lncol [0] = 0;
    shgain_geom._x0 = 68;
    shgain_geom._y0 = 17;
    shgain_geom._dx = 25;
    shgain_geom._dy = 25;
    shgain_geom._xref = 12.5;
    shgain_geom._yref = 12.5;
    shgain_geom._rad = 12;
 
    shfreq_geom._backg = XftColors [C_MAIN_BG];
    shfreq_geom._image [0] = shuffsect;
    shfreq_geom._lncol [0] = 0;
    shfreq_geom._x0 = 127;
    shfreq_geom._y0 = 17;
    shfreq_geom._dx = 25;
    shfreq_geom._dy = 25;
    shfreq_geom._xref = 12.5;
    shfreq_geom._yref = 12.5;
    shfreq_geom._rad = 12;
 
    lffreq_geom._backg = XftColors [C_MAIN_BG];
    lffreq_geom._image [0] = lfshfsect;
    lffreq_geom._lncol [0] = 0;
    lffreq_geom._x0 = 14;
    lffreq_geom._y0 = 31;
    lffreq_geom._dx = 25;
    lffreq_geom._dy = 25;
    lffreq_geom._xref = 12.5;
    lffreq_geom._yref = 12.5;
    lffreq_geom._rad = 12;

    lfgain_geom._backg = XftColors [C_MAIN_BG];
    lfgain_geom._image [0] = lfshfsect;
    lfgain_geom._lncol [0] = 1;
    lfgain_geom._x0 = 63;
    lfgain_geom._y0 = 17;
    lfgain_geom._dx = 25;
    lfgain_geom._dy = 25;
    lfgain_geom._xref = 12.5;
    lfgain_geom._yref = 12.5;
    lfgain_geom._rad = 12;

    return 0;
}


void styles_fini (X_display *disp)
{
}
