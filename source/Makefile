# ----------------------------------------------------------------------------
#
#  Copyright (C) 2010-2024 Fons Adriaensen <fons@linuxaudio.org>
#    
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------


PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
SHARED ?= $(PREFIX)/share/zita-bls1

VERSION = 0.4.0
CPPFLAGS += -MMD -MP -DVERSION=\"$(VERSION)\" -DSHARED=\"$(SHARED)\"
CXXFLAGS += -O2 -Wall -ffast-math -pthread 
CXXFLAGS += -march=native


all:	zita-bls1


ZITA-BLS1_O = zita-bls1.o styles.o jclient.o mainwin.o png2img.o guiclass.o rotary.o \
	hp3filt.o lfshelf2.o shuffler.o
zita-bls1:	CPPFLAGS += $(shell pkgconf --cflags freetype2)
zita-bls1:	LDLIBS += -lzita-convolver -lfftw3f -lclxclient -lclthreads -ljack \
	-lcairo -lpthread -lpng -lXft -lX11 -lrt
zita-bls1:	$(ZITA-BLS1_O)
	$(CXX) $(LDFLAGS) -o $@ $(ZITA-BLS1_O) $(LDLIBS)
$(ZITA-BLS1_O):
-include $(ZITA-BLS1_O:%.o=%.d)


install:	all
	install -d $(DESTDIR)$(BINDIR)
	install -d $(DESTDIR)$(SHARED)
	install -m 755 zita-bls1 $(DESTDIR)$(BINDIR)
	rm -rf $(DESTDIR)$(SHARED)/*
	install -m 644 ../share/* $(DESTDIR)$(SHARED)


uninstall:
	rm -f  $(DESTDIR)$(BINDIR)/zita-bls1
	rm -rf $(DESTDIR)$(SHARED)


clean:
	/bin/rm -f *~ *.o *.a *.d *.so
	/bin/rm -f zita-bls1

