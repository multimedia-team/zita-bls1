zita-bls1 (0.4.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable)

  [ Dennis Braun ]
  * New upstream version 0.4.0
  * Refresh patchset
  * Bump dh-compat to 13
  * Bump Standards-Version to 4.7.0
  * Prefer libjack-dev over libjack-jackd2-dev
  * Change B-D from pkg-config to pkgconf
  * Change my email address
  * Remove unneeded linker flags

 -- Dennis Braun <snd@debian.org>  Sun, 06 Oct 2024 12:02:01 +0200

zita-bls1 (0.3.3-3) unstable; urgency=medium

  * Upload to unstable
  * Install the icon to /usr/share/icons/hicolor/scalable/apps
  * Remove unnecessary dh argument -Smakefile

 -- Dennis Braun <d_braun@kabelmail.de>  Wed, 04 Mar 2020 14:52:29 +0100

zita-bls1 (0.3.3-2) experimental; urgency=medium

  * Build with libzita-convolver4 (Closes: #950809)
  * Add me as uploader

 -- Dennis Braun <d_braun@kabelmail.de>  Sat, 15 Feb 2020 17:52:07 +0100

zita-bls1 (0.3.3-1) unstable; urgency=medium

  * Team upload

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Olivier Humbert ]
  * Update http > https for d/copyright and d/watch
  * d/zita-bls1.desktop: Add French GenericName and Comment

  [ Dennis Braun ]
  * New upstream release 0.3.3
  * Add a bigger icon
  * d/control:
    + Bump debhelper-compat to 12
    + Bump standards version to 4.5.0
    + Use https protocol for homepage
    + Add libfreetype6-dev and pkg-config to B-D
    + Set RRR: no
  * d/copyright: Update year and add myself
  * d/patches:
    + Rename 01-fix_prefix.patch to 01-makefile.patch & update (Closes: #920875)
    + Add fixed pkconfig installation
    + Fix shuffler build
  * d/source/local-options: Drop, obsolete

 -- Dennis Braun <d_braun@kabelmail.de>  Mon, 03 Feb 2020 20:30:22 +0100

zita-bls1 (0.1.0-3) unstable; urgency=medium

  * Set dh/compat 10.
  * Update copyright file.
  * Sign tags.
  * Bump Standards.
  * Fix VCS fields.
  * Fix hardening.
  * Avoid useless linking.
  * Add local-options file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 27 Dec 2016 13:29:40 +0100

zita-bls1 (0.1.0-2) unstable; urgency=medium

  * Team upload.
  * debian/control:
    - Change libpng12-dev to libpng-dev. (Closes: #810207)
    - Update Standards-Version.
    - Update Vcs-Browser.
  * debian/menu: Removed to fix command-in-menu-file-and-desktop-file.

 -- Sebastian Ramacher <sramacher@debian.org>  Fri, 08 Jan 2016 21:36:51 +0100

zita-bls1 (0.1.0-1) unstable; urgency=low

  * Initial release (Closes: #719006).

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Mon, 19 Aug 2013 16:16:23 +0200
